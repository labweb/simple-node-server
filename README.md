## Install ##

Make sure you have ```node``` and ```npm``` installed.


```
#!bash

git clone git@bitbucket.org:labweb/simple-node-server.git
cd simple-node-server
npm install
```

## Run the server ##


```
#!bash

node index.js
```

You may also wish to install something like forever to run the the server after you log out.

