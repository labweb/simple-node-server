
// Replace these defaults with required settings
var settings = {
    port: 3000
};


var express = require('express');
var app = express();

var bodyParser = require('body-parser');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.get('/', function(req, res) {
    res.send('Hello World');
});

//example post
app.post('/update', function(req, res) {

});

// example static
app.use('/docs', express.static(__dirname + '/docs'));

app.listen(settings.port);
console.log('Server listening on port ' + settings.port);